<?php


namespace RbcParser\Connection;


use Exception;
use PDO;

class PdoConnection
{
    /** @var PDO */
    private $pdo;


    /**
     * Создаем экземпляр PDO
     *
     * @return PDO
     * @throws Exception
     */
    public function connect(): PDO
    {
        if ($this->pdo == null) {
            $this->pdo = new PDO("sqlite:" . __DIR__ . "/../../data/db/news.db", "","",array(
                PDO::ATTR_PERSISTENT => true,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ));
        }
        return $this->pdo;
    }

}