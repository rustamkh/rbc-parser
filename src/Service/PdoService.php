<?php


namespace RbcParser\Service;


use PDO;
use RbcParser\Helpers\ContentHelper;

class PdoService
{

    private $pdo;


    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }


    /**
     * Создаем структуру для хранения новостей
     * Мы, предварительно, очищаем базу, чтобы при каждом composer install / update она была наполнена
     * актуальными, не повторяющимися, данными
     */
    public function createTables():void
    {
        file_put_contents(__DIR__ . "/../../data/db/news.db", '');

        $this->pdo->exec(
            'CREATE TABLE IF NOT EXISTS news (
            id INTEGER PRIMARY KEY,
            title  VARCHAR (255) NOT NULL,
            content TEXT NOT NULL,
            image  VARCHAR (255) NULL
            )'
        );
    }


    /**
     * Сохраняем новости
     *
     * @param array $news
     */
    public function insertData(array $news):void
    {
        $sql = "INSERT INTO news (title, content, image) VALUES (:title,:content,:image)";

        $stmt = $this->pdo->prepare($sql);

        foreach($news as $row)
        {
            if (empty($row['title']) || empty($row['content'])) {
                continue;
            }
            $stmt->execute($row);
        }
    }


    /**
     * Получаем список новостей
     *
     * @return array
     */
    public function getAll():array
    {
        $stmt = $this->pdo->query("SELECT * FROM news");
        $news = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $news[] = $row;
        }

        return $news;
    }


    /**
     * Получаем одну новость
     *
     * @param int $id
     * @return array
     */
    public function getOne(int $id):array
    {
        $sql = "SELECT title, content, image FROM news WHERE id LIKE :id";

        $stmt = $this->pdo->prepare($sql);
        $stmt->execute([':id' => $id]);
        $news = [];

        if (empty($rows = $stmt->fetchAll(\PDO::FETCH_ASSOC))) {
            return $news;
        }

        foreach ($rows as $row) {
            $news = $row;
        }

        return $news;
    }

}