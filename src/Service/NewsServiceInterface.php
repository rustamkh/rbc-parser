<?php


namespace RbcParser\Service;


interface NewsServiceInterface
{
    /**
     * NewsServiceInterface constructor.
     * @param PdoService $pdoService
     */
    public function __construct(PdoService $pdoService);


    /**
     * Наполняем базу новостями
     */
    public function parseNews():void ;


    /**
     * Получаем список новостей
     * @return array
     */
    public function getAllNews():array ;


    /**
     * Получаем одну новость
     * @param int $id
     * @return array
     */
    public function getOneNews(int $id):array ;
}