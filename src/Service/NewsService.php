<?php


namespace RbcParser\Service;


use phpQuery;
use RbcParser\Helpers\ContentHelper;

class NewsService implements NewsServiceInterface
{
    // Ссылка на список актальных новостей. Обратите внимание, что можно увеличить количество элементов
    const NEWS_LINK_TEMPLATE = "https://www.rbc.ru/v10/ajax/get-news-feed/project/rbcnews.spb_sz/lastDate/%s/limit/15";

    /** @var PdoService  */
    private $pdoService;


    /**
     * NewsService constructor.
     * @param PdoService $pdoService
     */
    public function __construct(PdoService $pdoService)
    {
        $this->pdoService = $pdoService;
    }


    /**
     * Наполняем базу новостями
     */
    public function parseNews():void
    {
        // Создаю структуру
        $this->pdoService->createTables();
        $json = $this->curl_get_contents(
            sprintf(self::NEWS_LINK_TEMPLATE, time())
        );

        $news = $this->parseList(
            json_decode($json, true)
        );
        // Записываю в базу
        $this->pdoService->insertData($news);
    }


    /**
     * Формируем массив новостей
     *
     * @param array $newsList
     * @return array
     */
    private function parseList(array $newsList):array
    {
        $newsItems = $newsList["items"];
        $news = [];

        foreach($newsItems as $newsItem){
            phpQuery::newDocument($newsItem['html']);
            $parseLink = pq(".js-news-feed-item ");
            $news[] = $this->parseElement(
                $parseLink->attr("href")
            );
        }

        // Чистим память
        phpQuery::unloadDocuments();

        return $news;
    }


    /**
     * Получаем данные новости
     *
     * @param string $url
     * @return array
     */
    private function parseElement(string $url):array
    {
        $newsData = [];
        $html = $this->curl_get_contents($url);
        phpQuery::newDocument($html);
        $mainContent = pq(".l-col-main");
        $newsData['title'] = ContentHelper::fullTrim(
            $mainContent->find('.js-slide-title')->text()
        );
        // Получаем текст новости, удаляемм лишние пробелы и переносы строк
        $newsData['content'] = ContentHelper::fullTrim(
            $mainContent->find('.article__text')->find('p')->text()
        );

        $newsData['image'] = $mainContent->find('.js-rbcslider-image')->attr("src") ?? null;

        return $newsData;
    }


    /**
     * Получение контента
     *
     * @param $url
     * @return bool|string
     */
    private function curl_get_contents($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        // Вставляем юзерагент, чтобы не получить 404
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36");
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }


    /**
     * Получаем список новостей
     *
     * @return array
     */
    public function getAllNews(): array
    {
        return $this->pdoService->getAll();
    }


    /**
     * Получаем одну новость
     *
     * @param int $id
     * @return array
     */
    public function getOneNews(int $id): array
    {
        return $this->pdoService->getOne($id);
    }
}