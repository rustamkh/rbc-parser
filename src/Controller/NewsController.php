<?php


namespace RbcParser\Controller;


use eftec\bladeone\BladeOne;
use RbcParser\Service\NewsServiceInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NewsController
{
    /** @var NewsServiceInterface */
    private $newsService;

    /** @var BladeOne */
    private $blade;


    /**
     * NewsController constructor.
     * @param NewsServiceInterface $newsService
     * @param BladeOne $bladeOne
     */
    public function __construct(NewsServiceInterface $newsService, BladeOne $bladeOne)
    {
        $this->newsService = $newsService;
        $this->blade = $bladeOne;
    }


    /**
     * Список новостей
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function listAction(Request $request)
    {
        $news = $this->newsService->getAllNews();
        $view = $this->blade->run('list', ['news' => $news]);

        return new Response($view);
    }


    /**
     * Новость детально
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function detailAction(Request $request)
    {
        $id = $request->get('id');

        if (empty($news = $this->newsService->getOneNews($id))) {
            $view = $this->blade->run(
                'notfound'
            );
        } else {
            $view = $this->blade->run(
                'element',
                [
                    'title' => $news['title'],
                    'content' => $news['content'],
                    'image' => $news['image']
                ]
            );
        }

        return new Response($view);
    }


    /**
     * Вызывается при инталле или апдейте компосера
     * Наполняет базу новостями или выводит ошибку
     *
     * @param Request $request
     * @return Response
     */
    public function parseAction(Request $request)
    {
        try {
            $this->newsService->parseNews();
            return new Response('success');
        } catch (\Exception $e) {
            return new Response($e->getMessage());
        }
    }


}