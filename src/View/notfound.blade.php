<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="error-template">
                <h1>
                    Опа!</h1>
                <h2>
                    404 Страница не найдена</h2>
                <div class="error-details">
                    Скорее всего, вы обратились к несуществующей новости :(
                </div>
                <br>
                <div class="error-actions">
                    <a href="/" class="btn btn-primary btn-md"><span class="glyphicon glyphicon-home"></span>
                        К списку новостей </a>
                </div>
            </div>
        </div>
    </div>
</div>