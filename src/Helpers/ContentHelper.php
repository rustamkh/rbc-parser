<?php


namespace RbcParser\Helpers;


class ContentHelper
{

    /**
     * Удаляю лишние проблелы и переносы строк
     *
     * @param string $text
     * @return string
     */
    public static function fullTrim(string $text)
    {
        return trim(preg_replace('/\s{2,}/', ' ', $text));
    }


    /**
     * Удаляем последний символ из строки
     *
     * @param string $text
     * @return false|string
     */
    public static function removeLastLetter(string $text)
    {
        return mb_substr($text, 0, -1);
    }
}