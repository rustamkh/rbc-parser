<?php

use eftec\bladeone\BladeOne;
use RbcParser\Connection\PdoConnection;
use RbcParser\Controller\NewsController;
use RbcParser\Service\NewsService;
use RbcParser\Service\PdoService;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

$containerBuilder = new ContainerBuilder();
$connection = (new PdoConnection())->connect();
$blade = new BladeOne(__DIR__ . '/../src/View');

$containerBuilder->set('connection', $connection);
$containerBuilder->set('blade', $blade);
$containerBuilder->register('service.pdo', PdoService::class)->addArgument($containerBuilder->get('connection'));
$containerBuilder->register('service.news', NewsService::class)->addArgument(new Reference('service.pdo'));
$containerBuilder->register('controller.news', NewsController::class)
    ->addArgument(new Reference('service.news'))
    ->addArgument(new Reference('blade'));


return $containerBuilder;