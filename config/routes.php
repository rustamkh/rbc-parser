<?php

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;


$containerBuilder = require_once 'container.php';

$routes = new RouteCollection();

$routes->add(
    'list',
    new Route(
        '/',
        [
            'controller' => $containerBuilder->get('controller.news'),
            'method' => 'list'
        ],
        [
        ],
        [],
        '',
        'Http',
        'Get'
    )
);


$routes->add(
    'detail',
    new Route(
        '/detail/{id}',
        [
            'controller' => $containerBuilder->get('controller.news'),
            'method' => 'detail'
        ],
        [
            'id' => '\d*'
        ],
        [],
        '',
        'Http',
        'Get'
    )
);


$routes->add(
    'parse',
    new Route(
        '/parse',
        [
            'controller' => $containerBuilder->get('controller.news'),
            'method' => 'parse'
        ],
        [],
        [],
        '',
        '',
        'Get'
    )
);


return $routes;