<?php

require_once __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;


try {
    register_shutdown_function(function () {
        $error = error_get_last();

        if ($error !== NULL && in_array($error['type'], array(E_ERROR, E_PARSE, E_CORE_ERROR, E_CORE_WARNING, E_COMPILE_ERROR, E_COMPILE_WARNING,E_RECOVERABLE_ERROR))) {
            $response = new Response($error['message'], 500);

            $response->send();
        }
    });

    /** @var RouteCollection $routes */
    $routes = include __DIR__ . '/../config/routes.php';

    $request = Request::createFromGlobals();

    $context = new RequestContext();
    $context->fromRequest($request);

    $matcher = new UrlMatcher($routes, $context);

    $parameters = $matcher->matchRequest($request);
    //var_dump($parameters['controller']);die();
    if (php_sapi_name() === 'cli' && isset($argv[1])) {
        $parameters['method'] = $argv[1];
    }
    // Чтобы было возможно передать параметры
    $request->attributes->add($parameters);

    $response = $parameters['controller']->{$parameters['method'] . 'Action'}($request);

} catch (ResourceNotFoundException $exception) {
    $response = new Response('Not found', 404);
} catch (Exception $exception) {
    $response = new Response('An error occurred', 500);
}


$response->send();